# Axual Client .NET Example Project

This example repository shows a typical use of the different types of clients available in the Axual Platform.
All examples use the same usecase and resources, making it easier to compare them.

## Example Applications

### Goals
For the examples several goals are identified

1. Produce and Consume events using Axual Client Proxy
2. Start a custom Axual Platform Test standalone, using custom Server and Client Certificates

### Client types
The examples using the 'Axual Client Proxy'- a low level client based on Kafka interfaces and programming model

### Project Structure
* axual-client-proxy-string-consumer\
  Consumes messages from the `string_applicationlog` stream
* axual-client-proxy-string-producer\
  Produces messages to the `string_applicationlog` stream

* example-common\
A library containing the settings/configurations shared by the example applications and standalone server
The Server and Client SSL Certificates are also stored here


## How to Run
Before running the example you need to run the standalone application:
https://docs.cloud.axual.io/client/5.4.1/how-to/standalone.html

Run the following script to set up the platform, the script will create a Multi Cluster Standalone (Docker) with 'string_applicationlog' stream name and register the runnable application.
```shell
docker run -ti --rm --name axual-standalone \
    -p 8080-8081:8080-8081 \
    -p 8082-8084:8082-8084 \
    -p 8092-8094:8092-8094 \
    -e SERVER_PORT=8080 \
    -e STANDALONE_TYPE=multi \
    -e STANDALONE_TENANT=axual \
    -e STANDALONE_INSTANCE=platform_standalone \
    -e STANDALONE_ENVIRONMENT=local \
    -e STANDALONE_TTL=1000 \
    -e STANDALONE_DISTRIBUTOR_DISTANCE=2 \
    -e STANDALONE_DISTRIBUTOR_TIMEOUT=20000 \
    -e STANDALONE_BIND_ADDRESS=0.0.0.0 \
    -e STANDALONE_ADVERTISED_ADDRESS=localhost \
    -e STANDALONE_ENDPOINT_PORT=8081 \
    -e STANDALONE_USE_ADVANCED_ACL=true \
    -e STANDALONE_ENABLE_DISTRIBUTION=true \
    -e STANDALONE_CLUSTERS_0_NAME=clusterA \
    -e STANDALONE_CLUSTERS_0_SCHEMA_REGISTRY_PORT=8082 \
    -e STANDALONE_CLUSTERS_0_ZOOKEEPER_PORT=8083 \
    -e STANDALONE_CLUSTERS_0_BROKER_PORT=8084 \
    -e STANDALONE_CLUSTERS_0_TOPIC_PATTERN='{tenant}-{instance}-{environment}-{topic}' \
    -e STANDALONE_CLUSTERS_0_GROUP_ID_PATTERN='{tenant}-{instance}-{environment}-{group}' \
    -e STANDALONE_CLUSTERS_1_NAME=clusterB \
    -e STANDALONE_CLUSTERS_1_SCHEMA_REGISTRY_PORT=8092 \
    -e STANDALONE_CLUSTERS_1_ZOOKEEPER_PORT=8093 \
    -e STANDALONE_CLUSTERS_1_BROKER_PORT=8094 \
    -e STANDALONE_CLUSTERS_1_TOPIC_PATTERN='{tenant}-{instance}-{environment}-{topic}' \
    -e STANDALONE_CLUSTERS_1_GROUP_ID_PATTERN='{tenant}-{instance}-{environment}-{group}' \
    -e STANDALONE_APPLICATIONS_0_APPLICATION_ID=io.axual.client.dotnet.example.string \
    -e STANDALONE_APPLICATIONS_0_ENVIRONMENT=local \
    -e STANDALONE_APPLICATIONS_0_ASSIGNED_CLUSTER=clusterA \
    -e STANDALONE_STREAMS_0_NAME=string_applicationlog \
    -e STANDALONE_STREAMS_0_IS_RAW_TOPIC=false \
    -e STANDALONE_STREAMS_0_KEY_CLASS=io.axual.client.test.Random \
    -e STANDALONE_STREAMS_0_VALUE_CLASS=io.axual.client.test.Random \
    -e STANDALONE_STREAMS_0_PARTITIONS=2 \
    -e STANDALONE_STREAMS_0_ENVIRONMENT=local \
    docker.axual.io/axual/platform-test-standalone:5.4.1
```

## License
Axual Client .NET Example is licensed under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0.txt).
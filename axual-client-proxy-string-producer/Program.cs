﻿//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Numerics;
using System.Threading;
using Axual.DotNetClient.Proxy.Proxies.Axual;
using Confluent.Kafka;
using io.axual.client.dotnet.example.common;

namespace io.axual.client.dotnet.example.@string.consumer.producer
{
    class Program
    {
        static void Main(string[] args)
        {
            BigInteger keyID = new BigInteger(0);
            var topicName = "string_applicationlog";

            var config = new AxualProducerConfig
            {
                ApplicationId = "io.axual.client.dotnet.example.string",
                EndPoint = new UriBuilder("http", ServerSettings.LISTEN_ADDRESS, ServerSettings.ENDPOINT_PORT).Uri,
                
                //Server verifies the identity of the client
                // (i.e. that the public key certificate provided by the client has been signed by a
                // CA trusted by the server).
                SslKeystorePassword = ServerSettings.KEYSTORE_PASSWORD,
                SslKeystoreLocation = ServerSettings.KEYSTORE_RESOURCE_PATH,
                SecurityProtocol = SecurityProtocol.Ssl,
                Environment = ServerSettings.ENVIRONMENT,
                Tenant = ServerSettings.TENANT,
                EnableSslCertificateVerification = true, 
                // Client verifies the identity of the broker
                SslCaLocation = ServerSettings.SSL_CA_PATH,
                SslEndpointIdentificationAlgorithm = SslEndpointIdentificationAlgorithm.None,
            };

            using (var producer = new AxualProducerBuilder<string, string>(config)
                .SetKeySerializer(Serializers.Utf8)
                .SetValueSerializer(Serializers.Utf8).Build())
            {
                Console.WriteLine();
                Console.WriteLine("---------------------------------------------------------------------------------");
                Console.WriteLine($"  Producer '{typeof(Program).Namespace}' producing on topic '{topicName}'.");
                Console.WriteLine("---------------------------------------------------------------------------------");
                Console.WriteLine("Ctrl-C to quit.\n");

                var cancelled = false;
                Console.CancelKeyPress += (_, e) => {
                    e.Cancel = true; // prevent the process from terminating.
                    cancelled = true;
                };
                

                while (!cancelled)
                {
                    Console.Write("> ");
                    
                    BigInteger key = keyID;
                    BigInteger val = keyID;
                    keyID++;
                    
                    try
                    {
                        producer.Produce(
                            topicName, new Message<string, string> { Key = key.ToString(), Value = val.ToString()});
                        Console.WriteLine($"Delivered message: <{key},{val}>");
                    }
                    catch (ProduceException<string, string> e)
                    {
                        Console.WriteLine($"failed to deliver message: {e.Message} [{e.Error.Code}]");
                    }
                    
                    Thread.Sleep(100);
                }

                // Since we are producing synchronously, at this point there will be no messages
                // in-flight and no delivery reports waiting to be acknowledged, so there is no
                // need to call producer.Flush before disposing the producer.
            }
        }
    }
}
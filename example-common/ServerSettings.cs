﻿//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System.IO;

namespace io.axual.client.dotnet.example.common
{
    public static class ServerSettings
    {
        private static readonly string ROOT_FOLDER =
            Directory.GetParent(System.Environment.CurrentDirectory).Parent.Parent.Parent.FullName;
            
        public static readonly string TENANT = "axual";
        public static readonly string ENVIRONMENT = "local";
        public static readonly string LISTEN_ADDRESS = "localhost";
        public static readonly int ENDPOINT_PORT = 8081;

        public static readonly string KEYSTORE_RESOURCE_PATH = 
            ROOT_FOLDER + "/example-common/resources/axual.client.keystore.p12";

        public static readonly string KEYSTORE_PASSWORD = "notsecret";
        public static readonly string SSL_CA_PATH = ROOT_FOLDER + "/example-common/resources/ca-root.cer";
    }
}
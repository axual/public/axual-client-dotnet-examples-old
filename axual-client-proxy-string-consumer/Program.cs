﻿//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Threading;
using Axual.DotNetClient.Proxy.Proxies.Axual;
using Confluent.Kafka;
using io.axual.client.dotnet.example.common;

namespace io.axual.client.dotnet.example.@string.consumer
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var topicName = "string_applicationlog";
            var config = new AxualConsumerConfig
            {
                ApplicationId = "io.axual.client.dotnet.example.string",
                GroupId = "csharp-consumer",
                EndPoint = new UriBuilder("http", ServerSettings.LISTEN_ADDRESS, ServerSettings.ENDPOINT_PORT).Uri,
                SecurityProtocol = SecurityProtocol.Ssl,
                SslKeystorePassword = ServerSettings.KEYSTORE_PASSWORD,
                SslKeystoreLocation = ServerSettings.KEYSTORE_RESOURCE_PATH,
                EnableSslCertificateVerification = true,
                // Client verifies the identity of the broker
                SslCaLocation = ServerSettings.SSL_CA_PATH,
                SslEndpointIdentificationAlgorithm = SslEndpointIdentificationAlgorithm.None,
                Environment = ServerSettings.ENVIRONMENT,
                Tenant = ServerSettings.TENANT
            };

            Console.WriteLine($"Started consumer, Ctrl-C to stop consuming");

            CancellationTokenSource cts = new CancellationTokenSource();
            Console.CancelKeyPress += (_, e) =>
            {
                e.Cancel = true; // prevent the process from terminating.
                cts.Cancel();
            };

            Run_Consume(config, topicName, cts.Token);
        }

        public static void Run_Consume(AxualConsumerConfig config, string topicName,
            CancellationToken cancellationToken)
        {
            using (var consumer = new AxualConsumerBuilder<string, string>(config)
                .SetKeyDeserializer(Deserializers.Utf8)
                .SetValueDeserializer(Deserializers.Utf8)
                .Build())
            {
                Console.WriteLine("------------------------------------------------------------------------------------");
                Console.WriteLine($"  Consumer '{typeof(Program).Namespace}' consuming from topic '{topicName}'.");
                Console.WriteLine("------------------------------------------------------------------------------------");

                consumer.Subscribe(topicName);

                try
                {
                    while (true)
                    {
                        try
                        {
                            Console.Write("> ");
                            var consumeResult = consumer.Consume(cancellationToken);

                            if (consumeResult.IsPartitionEOF)
                            {
                                Console.WriteLine(
                                    $"Reached end of topic {consumeResult.Topic}, partition " +
                                    $"{consumeResult.Partition}, offset {consumeResult.Offset}.");

                                continue;
                            }

                            Console.WriteLine(
                                $"Received message at {consumeResult.TopicPartitionOffset}: " +
                                $"<{consumeResult.Key},{consumeResult.Value}>");

                            try
                            {
                                consumer.Commit(consumeResult);
                            }
                            catch (KafkaException e)
                            {
                                Console.WriteLine($"Commit error: {e.Error.Reason}");
                            }
                        }
                        catch (ConsumeException e)
                        {
                            Console.WriteLine($"Consume error: {e.Error.Reason}");
                        }
                    }
                }
                catch (OperationCanceledException)
                {
                    Console.WriteLine("Closing consumer.");
                    consumer.Close();
                }
            }
        }
    }
}